package com.sra.snama.sensometer;

import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;

import com.sra.snama.sensometer.service.AcceleratorService;
import com.sra.snama.sensometer.service.BarometerService;
import com.sra.snama.sensometer.service.GyroService;
import com.sra.snama.sensometer.service.HomeWatcher;
import com.sra.snama.sensometer.service.MagneticFieldService;


/**
 * Created by s.nama on 7/13/2015.
 */
public class ServiceLoader {
    private final String TAG = Common.TAG + "Loader";
    private Context mContext;
    private static PowerManager.WakeLock wakeLock;
    private static ServiceLoader mLoader;
    private ServiceLoader(Context ctx) {
        mContext = ctx;
        PowerManager powerManager = (PowerManager) ctx.getSystemService(Context.POWER_SERVICE);
        if(wakeLock==null)
            wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "sensometerlock");
    }
    public static ServiceLoader getInstance(Context ctx)
    {
        if(mLoader==null)
            mLoader=new ServiceLoader(ctx);
        return mLoader;
    }
    public void Start() {
        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                wakeLock.acquire();
                mContext.startService(new Intent(mContext, HomeWatcher.class));
                mContext.startService(new Intent(mContext, AcceleratorService.class));
                mContext.startService(new Intent(mContext, GyroService.class));
                mContext.startService(new Intent(mContext, MagneticFieldService.class));
                mContext.startService(new Intent(mContext, BarometerService.class));
               /* mContext.startService(new Intent(mContext, LightService.class));
                mContext.startService(new Intent(mContext, LinearAccService.class));
                mContext.startService(new Intent(mContext, StepService.class));
                mContext.startService(new Intent(mContext, ProximityService.class));
                mContext.startService(new Intent(mContext, GravityService.class));
                mContext.startService(new Intent(mContext, RotationVectorService.class));
                mContext.startService(new Intent(mContext, GameRotationService.class));
                mContext.startService(new Intent(mContext, WifiManagerService.class));
               */
            }
        };

        runnable.run();

    }

    public void Stop() {
        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                if(wakeLock.isHeld())
                    wakeLock.release();
                mContext.stopService(new Intent(mContext, HomeWatcher.class));
                mContext.stopService(new Intent(mContext, AcceleratorService.class));
                mContext.stopService(new Intent(mContext, GyroService.class));
                mContext.stopService(new Intent(mContext, MagneticFieldService.class));
                mContext.stopService(new Intent(mContext, BarometerService.class));
/*              mContext.stopService(new Intent(mContext, LightService.class));
                mContext.stopService(new Intent(mContext, LinearAccService.class));
                mContext.stopService(new Intent(mContext, ProximityService.class));
                mContext.stopService(new Intent(mContext, StepService.class));
                mContext.stopService(new Intent(mContext, GravityService.class));
                mContext.stopService(new Intent(mContext, RotationVectorService.class));
                mContext.stopService(new Intent(mContext, GameRotationService.class));
                mContext.stopService(new Intent(mContext, WifiManagerService.class));
*/
            }
        };

        runnable.run();

    }

}
